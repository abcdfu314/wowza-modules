package org.example.wowza.templates;

import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.client.IClient;
import com.wowza.wms.httpstreamer.cupertinostreaming.httpstreamer.HTTPStreamerSessionCupertino;
import com.wowza.wms.httpstreamer.model.IHTTPStreamerSession;
import com.wowza.wms.httpstreamer.smoothstreaming.httpstreamer.HTTPStreamerSessionSmoothStreamer;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.request.RequestFunction;
import com.wowza.wms.rtp.model.RTPSession;
import com.wowza.wms.stream.IMediaStream;

public class TemplateModule extends ModuleBase {

    public void exampleCustomMethod(IClient client, RequestFunction function, AMFDataList params) {
        getLogger().info("exampleCustomMethod");
        sendResult(client, params, "Hello Wowza");
    }

    public void onAppStart(IApplicationInstance appInstance) {
        String fullname = appInstance.getApplication().getName() + "/" + appInstance.getName();
        getLogger().info("onAppStart: " + fullname);
    }

    public void onAppStop(IApplicationInstance appInstance) {
        String fullname = appInstance.getApplication().getName() + "/" + appInstance.getName();
        getLogger().info("onAppStop: " + fullname);
    }

    public void onConnect(IClient client, RequestFunction function, AMFDataList params) {
        getLogger().info("onConnect: " + client.getClientId());
    }

    public void onConnectAccept(IClient client) {
        getLogger().info("onConnectAccept: " + client.getClientId());
    }

    public void onConnectReject(IClient client) {
        getLogger().info("onConnectReject: " + client.getClientId());
    }

    public void onDisconnect(IClient client) {
        getLogger().info("onDisconnect: " + client.getClientId());
    }

    public void onStreamCreate(IMediaStream stream) {
        getLogger().info("onStreamCreate: " + stream.getSrc());
    }

    public void onStreamDestroy(IMediaStream stream) {
        getLogger().info("onStreamDestroy: " + stream.getSrc());
    }

    public void onHTTPSessionCreate(IHTTPStreamerSession httpSession) {
        getLogger().info("onHTTPSessionCreate: " + httpSession.getSessionId());
    }

    public void onHTTPSessionDestroy(IHTTPStreamerSession httpSession) {
        getLogger().info("onHTTPSessionDestroy: " + httpSession.getSessionId());
    }

    public void onHTTPCupertinoStreamingSessionCreate(HTTPStreamerSessionCupertino httpSession) {
        getLogger().info("onHTTPCupertinoStreamingSessionCreate: " + httpSession.getSessionId());
    }

    public void onHTTPCupertinoStreamingSessionDestroy(HTTPStreamerSessionCupertino httpSession) {
        getLogger().info("onHTTPCupertinoStreamingSessionDestroy: " + httpSession.getSessionId());
    }

    public void onHTTPSmoothStreamingSessionCreate(HTTPStreamerSessionSmoothStreamer httpSession) {
        getLogger().info("onHTTPSmoothStreamingSessionCreate: " + httpSession.getSessionId());
    }

    public void onHTTPSmoothStreamingSessionDestroy(HTTPStreamerSessionSmoothStreamer httpSession) {
        getLogger().info("onHTTPSmoothStreamingSessionDestroy: " + httpSession.getSessionId());
    }

    public void onRTPSessionCreate(RTPSession rtpSession) {
        getLogger().info("onRTPSessionCreate: " + rtpSession.getSessionId());
    }

    public void onRTPSessionDestroy(RTPSession rtpSession) {
        getLogger().info("onRTPSessionDestroy: " + rtpSession.getSessionId());
    }

    public void onCall(String handlerName, IClient client, RequestFunction function, AMFDataList params) {
        getLogger().info("onCall: " + handlerName);
    }

}
