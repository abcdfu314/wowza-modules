package org.example.wowza.templates;

import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.server.IServer;
import com.wowza.wms.server.IServerNotify2;

public class TemplateServerListener implements IServerNotify2 {

    @Override
    public void onServerConfigLoaded(IServer server) {
        WMSLoggerFactory.getLogger(null).info("onServerConfigLoaded");
    }

    @Override
    public void onServerCreate(IServer server) {
        WMSLoggerFactory.getLogger(null).info("onServerCreate");
    }

    @Override
    public void onServerInit(IServer server) {
        WMSLoggerFactory.getLogger(null).info("onServerInit");
    }

    @Override
    public void onServerShutdownStart(IServer server) {
        WMSLoggerFactory.getLogger(null).info("onServerShutdownStart");
    }

    @Override
    public void onServerShutdownComplete(IServer server) {
        WMSLoggerFactory.getLogger(null).info("onServerShutdownComplete");
    }

}
