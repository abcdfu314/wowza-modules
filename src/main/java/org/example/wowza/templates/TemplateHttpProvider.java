package org.example.wowza.templates;

import com.wowza.wms.http.HTTProvider2Base;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.vhost.IVHost;
import java.io.OutputStream;

public class TemplateHttpProvider extends HTTProvider2Base {

    @Override
	public void onHTTPRequest(IVHost vhost, IHTTPRequest req, IHTTPResponse resp) {
		if (!doHTTPAuthentication(vhost, req, resp)) {
			return;
		}

        String helloStr = "Hello World!";
        String retStr = "<html><head><title>" + helloStr + "</title></head><body>" + helloStr + "</body></html>";

        try {
            OutputStream out = resp.getOutputStream();
            byte[] outBytes = retStr.getBytes();
            out.write(outBytes);
        } catch (Exception e) {
            WMSLoggerFactory.getLogger(null).error("ExampleHttpProvider: " + e.toString());
        }

    }

}
