# Wowza Modules

## Getting started

This is an example project of how to develop Wowza's extenssion without Wowza IDE.
In our case it's a gradle project.
Basically it's an alternative to [officially recommended way](https://www.wowza.com/docs/how-to-extend-wowza-streaming-engine-using-the-wowza-ide)...

### Add links to Wowza libs for your Wowza Streaming Engine

- Place content of `WowzaStreamingEngine/lib` directory into `libs` folder, or link them to the project in your own way.

### Examples

- There are three dummy templates provided:
    - TemplateHttpProvider
    - TemplateModule
    - TemplateServerListener
- There are some examples from official documentation provided:
    - HTTPConnectionCountsXML ([official docs](https://www.wowza.com/docs/how-to-get-connection-counts-for-server-applications-application-instances-and-streams-with-an-http-provider))
    - HTTPServerInfoXML ([official docs](https://www.wowza.com/docs/how-to-get-detailed-server-info-with-an-http-provider))
    - HttpCustomResponseCode ([official docs](https://www.wowza.com/docs/how-to-set-a-default-custom-http-response-code-httpcustomresponsecode)) 

### Build and release

- Just execute `gradle jar` goal
- Keep in mind that you should compile using the same target version of bytecode as Wowza uses (in my case it's 11)