package org.example.wowza.examples;

import com.wowza.wms.http.HTTProvider2Base;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.vhost.IVHost;

public class HTTPCustomResponseCode extends HTTProvider2Base {

    @Override
    public void onHTTPRequest(IVHost vhost, IHTTPRequest req, IHTTPResponse resp) {
		if (!doHTTPAuthentication(vhost, req, resp)) {
			return;
		}
        int responseCode = this.properties.getPropertyInt("responseCode", 200);
        resp.setResponseCode(responseCode);
    }
}
